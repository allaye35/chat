import nltk
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import nltk
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

# Télécharger les ressources nécessaires pour NLTK
nltk.download('punkt')
nltk.download('wordnet')  # Ajoutez cette ligne

# Le reste de votre code...

# Télécharger les ressources nécessaires pour NLTK
nltk.download('punkt')

# Initialiser le lemmatizer
lemmatizer = WordNetLemmatizer()

# Définir les réponses prédéfinies du chatbot
responses = {
    "Bonjour": "Bonjour ! Comment puis-je vous aider aujourd'hui ?",
    "Quelle est votre activité principale?": "Amadeus est une entreprise de conseil en systèmes et logiciels informatiques.",
    "Combien de salariés travaillent chez Amadeus?": "Amadeus compte entre 2000 et 4999 salariés.",
    "Quel est le chiffre d'affaires annuel d'Amadeus?": "Le chiffre d'affaires annuel d'Amadeus en 2022 est de 810 900 000,00 euros.",
    # Ajouter d'autres réponses prédéfinies ici
}

# Fonction pour prétraiter le texte
def preprocess_text(text):
    text = text.lower()
    tokens = nltk.word_tokenize(text)
    lemmatized_tokens = [lemmatizer.lemmatize(token) for token in tokens]
    preprocessed_text = ' '.join(lemmatized_tokens)
    return preprocessed_text

# Fonction pour obtenir la réponse du chatbot
def get_response(user_input):
    preprocessed_input = preprocess_text(user_input)
    responses['user_input'] = preprocessed_input
    response_sentences = list(responses.values())
    vectorizer = TfidfVectorizer()
    tfidf_matrix = vectorizer.fit_transform(response_sentences)
    similarity_scores = cosine_similarity(tfidf_matrix[-1], tfidf_matrix)
    most_similar_index = similarity_scores.argsort()[0][-2]
    response = response_sentences[most_similar_index]
    del responses['user_input']
    return response

# Boucle principale du chatbot
def chat():
    print("Bonjour ! Je suis le chatbot d'Amadeus. Comment puis-je vous aider aujourd'hui ?")
    while True:
        user_input = input("> ")
        if user_input.lower() == "quitter":
            break
        response = get_response(user_input)
        print(response)

# Lancer le chatbot
if __name__ == "__main__":
    chat()
